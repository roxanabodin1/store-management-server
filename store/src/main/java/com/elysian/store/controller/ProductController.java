package com.elysian.store.controller;

import com.elysian.store.security.Roles;
import com.elysian.store.security.Roles.*;
import com.elysian.store.model.Product;
import com.elysian.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(final ProductService productService) {
        this.productService = productService;
    }

    @Secured({Roles.ADMIN_ROLE})
    @PostMapping
    public ResponseEntity<String> create(@RequestBody Product product) {
        String message = productService.create(product);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully added product!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.ADMIN_ROLE})
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable final int id) {
        Optional<Product> product = productService.get(id);
        if (product.isPresent()) {
            return new ResponseEntity<>(product, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("No product found with id " + id, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.ADMIN_ROLE})
    @GetMapping
    public List<Product> getAll(){
        return productService.getAll();
    }

    @Secured({Roles.ADMIN_ROLE})
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable final int id, @RequestBody Product product) {
        String message = productService.update(id, product);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully update product!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.MANAGER_ROLE})
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable final int id) {
        String message = productService.delete(id);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully deleted product!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }
}
