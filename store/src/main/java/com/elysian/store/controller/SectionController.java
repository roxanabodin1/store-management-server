package com.elysian.store.controller;

import com.elysian.store.config.SectionSetup;
import com.elysian.store.model.Product;
import com.elysian.store.model.Section;
import com.elysian.store.security.Roles;
import com.elysian.store.service.ProductService;
import com.elysian.store.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/section")
public class SectionController {

    private final SectionService sectionService;

    @Autowired
    public SectionController(final SectionService sectionService) {
        this.sectionService = sectionService;
    }

    @Secured({Roles.ADMIN_ROLE})
    @PostMapping
    public ResponseEntity<String> create(@RequestBody Section section) {
        String message = sectionService.create(section);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully added section!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.ADMIN_ROLE})
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable final int id) {
        Optional<Section> section = sectionService.get(id);
        if (section.isPresent()) {
            return new ResponseEntity<>(section, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("No section found with id " + id, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.ADMIN_ROLE})
    @GetMapping
    public List<Section> getAll() {
        return sectionService.getAll();
    }

    @Secured({Roles.ADMIN_ROLE})
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable final int id, @RequestBody Section Section) {
        String message = sectionService.update(id, Section);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully update section!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.MANAGER_ROLE})
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable final int id) {
        String message = sectionService.delete(id);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully deleted section!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }
}
