package com.elysian.store.controller;

import com.elysian.store.model.Store;
import com.elysian.store.security.Roles;
import com.elysian.store.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/store")
public class StoreController {

    private final StoreService storeService;

    @Autowired
    public StoreController(final StoreService storeService) {
        this.storeService = storeService;
    }

    @Secured({Roles.ADMIN_ROLE})
    @PostMapping
    public ResponseEntity<String> create(@RequestBody Store store) {
        String message = storeService.create(store);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully added Store!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.ADMIN_ROLE})
    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable final int id) {
        Optional<Store> Store = storeService.get(id);
        if (Store.isPresent()) {
            return new ResponseEntity<>(Store, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("No Store found with id " + id, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.MANAGER_ROLE})
    @GetMapping
    public List<Store> getAll() {
        return storeService.getAll();
    }

    @Secured({Roles.ADMIN_ROLE})
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable final int id, @RequestBody Store Store) {
        String message = storeService.update(id, Store);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully update Store!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @Secured({Roles.MANAGER_ROLE})
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable final int id) {
        String message = storeService.delete(id);
        if (message.isEmpty()) {
            return new ResponseEntity<>("Successfully deleted Store!", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }
}
