package com.elysian.store.model;

public enum StoreSection {
    Tablets,
    Monitors,
    Laptops,
    MokedSection
}
