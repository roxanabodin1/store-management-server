package com.elysian.store.repository;

import com.elysian.store.config.ProductsSetup;
import com.elysian.store.config.SectionSetup;
import com.elysian.store.model.Section;
import com.elysian.store.model.StoreSection;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class SectionRepository {

    // an in-memory list of Sections
    private List<Section> sections = SectionSetup.getSections();

    public Optional<Section> get(int id) {
        try {
            Optional<Section> section = Optional.ofNullable(sections.get(id));
            return section;
        } catch (IndexOutOfBoundsException e){
           System.out.printf("The index %d is out of bounds!", id);
        }
        return Optional.empty();
    }

    public List<Section> getAll() {
        return sections;
    }

    public String create(final Section section) {
        try {
            sections.add(section);
            return "";
        } catch (Exception e){
            return "Could not add section, an exception occurred: " + e;
        }
    }

    public String update(final int id, final Section section) {
        try {
            sections.set(id, section);
            return "";
        } catch (Exception e){
            return "Could not update section, an exception occurred: " + e;
        }
    }

    public String delete(final int id) {
        try {
            sections.remove(id);
            return "";
        } catch (Exception e){
            return "Could not delete section, an exception occurred: " + e;
        }
    }
}
