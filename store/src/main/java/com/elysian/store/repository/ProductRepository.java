package com.elysian.store.repository;

import com.elysian.store.config.ProductsSetup;
import com.elysian.store.model.Product;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepository {

    // an in-memory list of products
    private List<Product> products = ProductsSetup.getProducts();

    public Optional<Product> get(int id) {
        try {
            Optional<Product> product = Optional.ofNullable(products.get(id));
            return product;
        } catch (IndexOutOfBoundsException e){
           System.out.printf("The index %d is out of bounds!", id);
        }
        return Optional.empty();
    }

    public List<Product> getAll() {
        return products;
    }

    public String create(final Product product) {
        try {
            products.add(product);
            return "";
        } catch (Exception e){
            return "Could not add product, an exception occurred: " + e;
        }
    }

    public String update(final int id, final Product product) {
        try {
            products.set(id, product);
            return "";
        } catch (Exception e){
            return "Could not update product, an exception occurred: " + e;
        }
    }

    public String delete(final int id) {
        try {
            products.remove(id);
            return "";
        } catch (Exception e){
            return "Could not delete product, an exception occurred: " + e;
        }
    }
}
