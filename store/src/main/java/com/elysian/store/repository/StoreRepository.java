package com.elysian.store.repository;

import com.elysian.store.config.StoreSetup;
import com.elysian.store.model.Store;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class StoreRepository {

    // an in-memory list of Stores
    private List<Store> stores = StoreSetup.getStores();

    public Optional<Store> get(int id) {
        try {
            Optional<Store> store = Optional.ofNullable(stores.get(id));
            return store;
        } catch (IndexOutOfBoundsException e){
           System.out.printf("The index %d is out of bounds!", id);
        }
        return Optional.empty();
    }

    public List<Store> getAll() {
        return stores;
    }

    public String create(final Store store) {
        try {
            stores.add(store);
            return "";
        } catch (Exception e){
            return "Could not add Store, an exception occurred: " + e;
        }
    }

    public String update(final int id, final Store store) {
        try {
            stores.set(id, store);
            return "";
        } catch (Exception e){
            return "Could not update Store, an exception occurred: " + e;
        }
    }

    public String delete(final int id) {
        try {
            stores.remove(id);
            return "";
        } catch (Exception e){
            return "Could not delete Store, an exception occurred: " + e;
        }
    }
}
