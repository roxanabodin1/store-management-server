package com.elysian.store.service;

import com.elysian.store.model.Section;
import com.elysian.store.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SectionService {

    private final SectionRepository sectionRepository;

    @Autowired
    public SectionService(final SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    public String create(final Section Section) {
       return sectionRepository.create(Section);
    }

    public Optional<Section> get(final int id) {
        return sectionRepository.get(id);
    }

    public List<Section> getAll() {
        return sectionRepository.getAll();
    }

    public String update(final int id, final Section Section) {
        return sectionRepository.update(id, Section);
    }

    public String delete(final int id) {
        return sectionRepository.delete(id);
    }
}
