package com.elysian.store.service;

import com.elysian.store.model.Store;
import com.elysian.store.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreService {

    private final StoreRepository storeRepository;

    @Autowired
    public StoreService(final StoreRepository storeRepository) { this.storeRepository = storeRepository;
    }

    public String create(final Store store) {
        return storeRepository.create(store);
    }

    public Optional<Store> get(final int id) {
        return storeRepository.get(id);
    }

    public List<Store> getAll() {
        return storeRepository.getAll();
    }

    public String update(final int id, final Store store) {
        return storeRepository.update(id, store);
    }

    public String delete(final int id) { return storeRepository.delete(id);
    }
}
