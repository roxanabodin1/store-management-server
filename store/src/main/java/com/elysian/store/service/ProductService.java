package com.elysian.store.service;

import com.elysian.store.model.Product;
import com.elysian.store.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductService(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public String create(final Product product) {
       return productRepository.create(product);
    }

    public Optional<Product> get(final int id) {
        return productRepository.get(id);
    }

    public List<Product> getAll() {
        return productRepository.getAll();
    }

    public String update(final int id, final Product product) {
        return productRepository.update(id, product);
    }

    public String delete(final int id) {
        return productRepository.delete(id);
    }
}
