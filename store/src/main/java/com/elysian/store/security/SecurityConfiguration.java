package com.elysian.store.security;

import com.elysian.store.security.handler.FailedAuthHandler;
import com.elysian.store.security.handler.PostLogoutHandler;
import com.elysian.store.security.handler.SuccessfulAuthHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.RememberMeConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.SimpleRedirectInvalidSessionStrategy;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true
)
@SuppressWarnings("unused")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String[] IGNORED_ENDPOINTS = {"/info", "/about"};

    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
            //            .passwordEncoder(passwordEncoder())
            .withUser("admin")
            // the unencrypted password is 'password'
            .authorities(Roles.ADMIN_ROLE)
            .password("$2a$10$4xnpk2a5jLr1mf6VWle6Vuv4q7DBsW2rqQcg6N1Ms/y4g98Ry4D4C")
            .and()
            .withUser("manager")
            .password("$2a$10$4xnpk2a5jLr1mf6VWle6Vuv4q7DBsW2rqQcg6N1Ms/y4g98Ry4D4C")
            .authorities(Roles.MANAGER_ROLE);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/static/**").permitAll()
            .antMatchers(HttpMethod.GET, "/product/*").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.POST, "/product").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.PUT, "/product").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.DELETE, "/product").hasAuthority(Roles.MANAGER_ROLE)

            .antMatchers(HttpMethod.GET, "/section/*").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.POST, "/section").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.PUT, "/section").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.DELETE, "/section").hasAuthority(Roles.MANAGER_ROLE)

            .antMatchers(HttpMethod.GET, "/store/*").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.POST, "/store").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.PUT, "/store").hasAuthority(Roles.ADMIN_ROLE)
            .antMatchers(HttpMethod.DELETE, "/store").hasAuthority(Roles.MANAGER_ROLE)
            .anyRequest().fullyAuthenticated();

        // registering the post auth handlers
        // they are registered as beans in order to be able to inject other dependencies in them (if needed)
        http.formLogin()
            .successHandler(successfulAuthHandler())
            .failureHandler(failedAuthHandler())
            .defaultSuccessUrl("/")
            .failureUrl("/login?error")
            .usernameParameter("username")
            .passwordParameter("password")
            .permitAll();

        http.csrf().disable();

        final RememberMeConfigurer<HttpSecurity> rememberMeConfigurer = http.rememberMe();
        rememberMeConfigurer.key("x");

        // registering the post logout handler
        http.logout()
            .deleteCookies("JSESSIONID")
            .clearAuthentication(true)
            .addLogoutHandler(postLogoutHandler());

        configureSessionManagement(http);
    }

    @Override
    public void configure(final WebSecurity web) {
        web.ignoring().antMatchers(IGNORED_ENDPOINTS);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    @Bean
    public SuccessfulAuthHandler successfulAuthHandler() {
        return new SuccessfulAuthHandler();
    }

    @Bean
    public FailedAuthHandler failedAuthHandler() {
        return new FailedAuthHandler();
    }

    @Bean
    public PostLogoutHandler postLogoutHandler() {
        return new PostLogoutHandler();
    }

    private void configureSessionManagement(HttpSecurity http) throws Exception {
        final SessionManagementConfigurer<HttpSecurity> sessionManagement = http.sessionManagement();
        sessionManagement.maximumSessions(3);
        sessionManagement.invalidSessionStrategy(new SimpleRedirectInvalidSessionStrategy("/login"));
        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
    }

}
