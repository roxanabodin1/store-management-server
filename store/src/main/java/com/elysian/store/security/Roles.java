package com.elysian.store.security;

public final class Roles {

    public static final String ADMIN_ROLE = "ROLE_ADMIN";

    public static final String MANAGER_ROLE = "ROLE_MANAGER";

    public static String getAdminRole() {
        return ADMIN_ROLE;
    }
}
