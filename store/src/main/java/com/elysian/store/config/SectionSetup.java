package com.elysian.store.config;

import com.elysian.store.model.Section;
import com.elysian.store.model.StoreSection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

// using an in-memory list of sections, until we'll connect to a database
@SuppressWarnings("unused")
public final class SectionSetup {

    private static final Random RANDOM = new Random(1000);

    private static final List<Section> sections;

    static {
        sections = buildSections();
    }

    public static List<Section> getSections() {
        return sections;
    }

    private SectionSetup() {
    }

    private static List<Section> buildSections() {
        return new ArrayList<>(Arrays.asList(
                new Section(0, StoreSection.Tablets, ProductsSetup.getTablets()),
                new Section(1, StoreSection.Monitors, ProductsSetup.getMonitors()),
                new Section(2, StoreSection.Laptops, ProductsSetup.getLaptops()))
        );
    }

}
