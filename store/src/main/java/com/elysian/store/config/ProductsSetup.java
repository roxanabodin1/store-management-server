package com.elysian.store.config;

import com.elysian.store.model.Discount;
import com.elysian.store.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

// using an in-memory list of products, until we'll connect to a database
@SuppressWarnings("unused")
public final class ProductsSetup {

    private static final Random RANDOM = new Random(1000);

    private static final List<Product> products;
    private static final List<Product> tablets;
    private static final List<Product> monitors;
    private static final List<Product> laptops;

    static {
        products = buildProducts();
        tablets = buildTablets();
        monitors = buildMonitors();
        laptops = buildLaptops();
    }

    public static List<Product> getProducts() {
        return products;
    }

    public static List<Product> getTablets() {
        return tablets;
    }

    public static List<Product> getMonitors() {
        return monitors;
    }

    public static List<Product> getLaptops() {
        return laptops;
    }

    private ProductsSetup() {
    }

    private static List<Product> buildProducts() {
        return new ArrayList<>(Arrays.asList(
                new Product(0, "Google Nexus 5", getRandomPrice(130), new Discount(20, Discount.Type.Percent)),
                new Product(1, "Google Nexus 7", getRandomPrice(150), new Discount(50, Discount.Type.Value)),
                new Product(2, "Apple iPad Pro", getRandomPrice(300), new Discount(10, Discount.Type.Percent)),
                new Product(3, "Samsung Galaxy Tab", getRandomPrice(200)),
                new Product(4, "Microsoft Surface Pro", getRandomPrice(230)),
                new Product(5, "Samsung CF791", getRandomPrice(700)),
                new Product(6, "Dell UP3218K", getRandomPrice(600)),
                new Product(7, "Samsung CH711", getRandomPrice(900)),
                new Product(8, "Lenovo Carbon X11", getRandomPrice(1000)),
                new Product(9, "Apple MacBookPro", getRandomPrice(1500)),
                new Product(10, "Razer Blade Black", getRandomPrice(2000)))
        );
    }

    private static List<Product> buildTablets() {
        return new ArrayList<>(Arrays.asList(
                new Product(1, "Google Nexus 7", getRandomPrice(150), new Discount(50, Discount.Type.Value)),
                new Product(2, "Apple iPad Pro", getRandomPrice(300), new Discount(10, Discount.Type.Percent)),
                new Product(3, "Samsung Galaxy Tab", getRandomPrice(200)),
                new Product(4, "Microsoft Surface Pro", getRandomPrice(230)))
                            );
    }

    private static List<Product> buildMonitors() {
        return new ArrayList<>(Arrays.asList(
                new Product(5, "Samsung CF791", getRandomPrice(700)),
                new Product(6, "Dell UP3218K", getRandomPrice(600)),
                new Product(7, "Samsung CH711", getRandomPrice(900)))
                            );
    }

    private static List<Product> buildLaptops() {
        return new ArrayList<>(Arrays.asList(
                new Product(8, "Lenovo Carbon X11", getRandomPrice(1000)),
                new Product(9, "Apple MacBookPro", getRandomPrice(1500)),
                new Product(10, "Razer Blade Black", getRandomPrice(2000)))
                            );
    }

    private static double getRandomPrice(final int multiplier) {
        return RANDOM.nextDouble() * multiplier;
    }
}
