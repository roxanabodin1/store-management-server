package com.elysian.store.config;

import com.elysian.store.model.Store;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

// using an in-memory list of stores, until we'll connect to a database
@SuppressWarnings("unused")
public final class StoreSetup {

    private static final Random RANDOM = new Random(1000);

    private static final List<Store> stores;

    static {
        stores = buildStores();
    }

    public static List<Store> getStores() {
        return stores;
    }

    private StoreSetup() {
    }

    private static List<Store> buildStores() {
        return new ArrayList<>(Arrays.asList(
                new Store(0, "Media Galaxy", "Timisoara", new HashSet<>(SectionSetup.getSections())))
        );
    }

}
