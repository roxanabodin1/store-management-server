package com.elysian.store;

import com.elysian.store.model.Product;
import com.elysian.store.model.Section;
import com.elysian.store.model.StoreSection;
import com.elysian.store.repository.SectionRepository;
import com.elysian.store.service.SectionService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
class SectionServiceTest {

    @Mock
    private SectionRepository sectionRepository;

    @InjectMocks
    private SectionService sectionService;

    @Test
    @DisplayName("Given there are available sections, when retrieving the sections then sections are retrieved " +
                 "correctly")
    void givenThereAreAvailableSections_whenRetrievingSections_thenSectionsAreRetrievedCorrectly() {
        final List<Section> sections = Arrays.asList(
                new Section(1, StoreSection.Tablets, List.of(new Product(1, "Google Nexus 7", 150))),
                new Section(2, StoreSection.Monitors, List.of(new Product(5, "Samsung CF791", 700)))
                );

        when(sectionRepository.getAll()).thenReturn(sections);

        final List<Section> allSections = sectionService.getAll();

        assertNotNull(allSections, "The sections are null");
        assertEquals(allSections.size(), sections.size(), "Not all the sections were returned");
        assertTrue(allSections.iterator().hasNext(), "There is just a single section");
        allSections.forEach(section -> {
            assertThat(section.getId(), not(0));
            assertThat("The name must not be null or empty", StringUtils.hasLength(section.getName().toString()));
        });
    }

    @Test
    @DisplayName("Given there are available sections, when retrieving a section by ID then the section is retrieved")
    void givenThereAreAvailableSections_whenRetrievingASectionById_thenTheSectionIsCorrectlyRetrieved() {
        final int sectionId = 2;

        final Section section = mock(Section.class);
        final StoreSection mockedName = StoreSection.MokedSection;
        when(section.getName()).thenReturn(mockedName);
        when(section.getId()).thenReturn(sectionId);

        when(sectionRepository.get(sectionId)).thenReturn(Optional.of(section));

        final Section resultedSection = sectionService.get(sectionId).get();

        assertNotNull(resultedSection);
        assertThat(resultedSection.getName(), is(mockedName));
        assertThat(resultedSection.getId(), not(0));
        assertThat(resultedSection.getId(), is(sectionId));
    }
}
