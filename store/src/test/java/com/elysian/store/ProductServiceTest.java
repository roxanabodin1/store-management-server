package com.elysian.store;

import com.elysian.store.model.Product;
import com.elysian.store.repository.ProductRepository;
import com.elysian.store.service.ProductService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService productService;

    @Test
    @DisplayName("Given there are available products, when retrieving the products then products are retrieved correctly")
    void givenThereAreAvailableProducts_whenRetrievingProducts_thenProductsAreRetrievedCorrectly() {
        final List<Product> products = Arrays.asList(
                new Product(2, "Asus", 100),
                new Product(5, "Dell", 200)
                                                    );
        when(productRepository.getAll()).thenReturn(products);

        final List<Product> allProducts = productService.getAll();

        assertNotNull(allProducts, "The products are null");
        assertEquals(allProducts.size(), products.size(), "Not all the products were returned");
        assertTrue(allProducts.iterator().hasNext(), "There is just a single product");
        allProducts.forEach(product -> {
        	assertThat(product.getId(), not(0));
			assertThat("The name must not be null or empty", StringUtils.hasLength(product.getName()));
        });
    }

    @Test
    @DisplayName("Given there are no available products, when retrieving the products then no products are retrieved")
    void givenThereAreNoAvailableProducts_whenRetrievingProducts_thenNoProductsAreReturned() {
        when(productRepository.getAll()).thenReturn(new ArrayList<>());

        final List<Product> allProducts = productService.getAll();

        assertNotNull(allProducts);
        assertThat(allProducts.size(), is(0));
    }
}
