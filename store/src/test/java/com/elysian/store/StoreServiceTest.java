package com.elysian.store;

import com.elysian.store.model.Product;
import com.elysian.store.model.Section;
import com.elysian.store.model.Store;
import com.elysian.store.model.StoreSection;
import com.elysian.store.repository.StoreRepository;
import com.elysian.store.service.StoreService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
class StoreServiceTest {

    @Mock
    private StoreRepository storeRepository;

    @InjectMocks
    private StoreService storeService;

    @Test
    @DisplayName("Given there are available stores, when retrieving the stores then stores are retrieved " +
                 "correctly")
    void givenThereAreAvailableStores_whenRetrievingStores_thenStoresAreRetrievedCorrectly() {
        final Set<Section> sections = new HashSet<>(Arrays.asList(
                new Section(1, StoreSection.Tablets, List.of(new Product(1, "Google Nexus 7", 150))),
                new Section(2, StoreSection.Monitors, List.of(new Product(5, "Samsung CF791", 700)))
                                                                 ));

        final List<Store> stores = Arrays.asList(
                new Store(1, "Media Galaxy", "Timisoara", sections),
                new Store(2, "Media Galaxy Cluj-Napoca", "Timisoara", sections)
                                                );

        when(storeRepository.getAll()).thenReturn(stores);

        final List<Store> allStores = storeService.getAll();

        assertNotNull(allStores, "The stores are null");
        assertEquals(allStores.size(), sections.size(), "Not all the stores were returned");
        allStores.forEach(store -> {
            assertThat(store.getId(), not(0));
            assertThat("The name must not be null or empty", StringUtils.hasLength(store.getName()));
        });
    }

    @Test
    @DisplayName("Given a store is created, when creating the store then create is called one time ")
    void givenAStoreIsCreated_whenCreatingTheStore_thenCreateIsCalledOneTime() {
        final Store store = mock(Store.class);

        storeService.create(store);

        verify(storeRepository, times(1)).create(any(Store.class));

    }
}
